<div align="center">
  <img src="./docs/assets/logo_inverse.png" alt="Logo" width="200">
</div>

# BF Instabot

## Requirements

- [Docker](https://www.docker.com/)
- [b5](https://github.com/team23/b5)

## Usage

### Local (b5)

1. Clone the Repository
```bash
mkdir ~/work
cd ~/work
git clone https://gitlab.com/LeonardSylvester/bfinstabot.git
```

2. Install dependencies and build docker image
```bash
cd ~/work/bfinstabot
b5 install
```

3. Run the Project
```bash
cd ~/work/bfinstabot
b5 run USERNAME PASSWORD PUT,TAGS,HERE LIKES_PER_DAY UNFOLLOW_PER_DAY WHITELISTED,USER,NAMES
```

### Everywhere else (no b5 required)
```bash
docker pull registry.gitlab.com/leonardsylvester/bfinstabot:latest
docker run -it \
    -e USERNAME="username" \
    -e PASSWORD="password123" \ 
    -e TAGS="put,your,tags,here" \ 
    -e LIKES_PER_DAY="100" \
    -e UNFOLLOW_PER_DAY="80" \ 
    -e UNFOLLOW_WHITELIST="put,your,usernames,here" \
    --name "instabot_USERNAME" \
    bfinstabot:latest
```

