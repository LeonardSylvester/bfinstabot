#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import os
import time
import json

from instabot_py import InstaBot
from instabot_py.check_status import check_status
from instabot_py.feed_scanner import feed_scanner
from instabot_py.follow_protocol import follow_protocol
from instabot_py.unfollow_protocol import unfollow_protocol


with open('config.json') as f:
    data = json.load(f)

LOGIN = os.getenv('USERNAME')
PASSWORD = os.getenv('PASSWORD')

TAG_LIST = os.getenv('TAG_LIST', None)
if TAG_LIST:
    TAG_LIST = TAG_LIST.split(',')
else:
    TAG_LIST = []
print('tag list: %s', TAG_LIST)

TAG_BLACKLIST = os.getenv('TAG_BLACKLIST', None)
if TAG_BLACKLIST:
    TAG_BLACKLIST = TAG_BLACKLIST.split(',')
else:
    TAG_BLACKLIST = []
print(f'tag blacklist: %s', TAG_BLACKLIST)

UNFOLLOW_WHITELIST = os.getenv('UNFOLLOW_WHITELIST', None)
if UNFOLLOW_WHITELIST:
    UNFOLLOW_WHITELIST = UNFOLLOW_WHITELIST.split(',')
else:
    UNFOLLOW_WHITELIST = []
print(f'unfollow whitelist: %s', UNFOLLOW_WHITELIST)


if data and LOGIN and PASSWORD:
    bot = InstaBot(
        login=LOGIN,
        password=PASSWORD,
        like_per_day=int(os.getenv('LIKE_PER_DAY', 1000)),
        comments_per_day=int(os.getenv('COMMNENTS_PER_DAY', 0)),
        tag_list=TAG_LIST,
        tag_blacklist=TAG_BLACKLIST,
        user_blacklist={},
        max_like_for_one_tag=int(os.getenv('MAX_LIKE_FOR_ONE_TAG', 50)),
        follow_per_day=int(os.getenv('FOLLOW_PER_DAY', 300)),
        follow_time=int(os.getenv('FOLLOW_TIME', 360)),
        unfollow_per_day=int(os.getenv('UNFOLLOW_PER_DAY', 300)),
        unlike_per_day=int(os.getenv('UNLIKE_PER_DAY', 0)),
        unfollow_recent_feed=os.getenv('UNFOLLOW_RECENT_FEED', True),
        time_till_unlike=int(os.getenv('TIME_TILL_UNLIKE', 259200)),
        unfollow_break_min=int(os.getenv('UNFOLLOW_BREAK_MIN', 15)),
        unfollow_break_max=int(os.getenv('UNFOLLOW_BREAK_MAX', 30)),
        user_max_follow=int(os.getenv('USER_MAX_FOLLOW', 0)),
        user_min_follow=int(os.getenv('USER_MIN_FOLLOW', 0)),
        log_mod=int(os.getenv('LOG_MODE', 0)),
        proxy=os.getenv('PROXY', ''),
        comment_list=data['comment_list'],
        unwanted_username_list=data['unwanted_username_list'],
        unfollow_whitelist=UNFOLLOW_WHITELIST
    )
    bot.mainloop()
